<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="A layout example that shows off a responsive product landing page.">

    <title>CHEWS</title>

    

<!--
<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
-->
<link rel="stylesheet" href="css/layouts/pure-css.css">

<!--[if lte IE 8]>
  
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-old-ie-min.css">
  
<![endif]-->
<!--[if gt IE 8]><!-->
  
    <link rel="stylesheet" href="css/layouts/pure-grid.css">
  
<!--<![endif]-->



<link rel="stylesheet" href="css/layouts/font-awesome.css">



  
    <!--[if lte IE 8]>
        <link rel="stylesheet" href="css/layouts/marketing-old-ie.css">
    <![endif]-->
    <!--[if gt IE 8]><!-->
        <link rel="stylesheet" href="css/layouts/marketing.css">
    <!--<![endif]-->
  


    

    

</head>
<body>









<div class="header">
    <div class="home-menu pure-menu pure-menu-horizontal pure-menu-fixed">
        <a class="pure-menu-heading" href="">CHEWS</a>

        <ul class="pure-menu-list">
            <li class="pure-menu-item pure-menu-selected"><a href="#" class="pure-menu-link">Home</a></li>
            <li class="pure-menu-item"><a href="#" class="pure-menu-link">CD</a></li>
            <li class="pure-menu-item"><a href="#" class="pure-menu-link">Report</a></li>
        </ul>
    </div>
</div>

<div class="splash-container">
    <div class="splash">
        <div class="splash-head"><img pure-img src="css/layouts/profile.png"></div>
        <p class="splash-subhead">
            Community Health Early Warning System
        </p>
        <p>
            <a href="http://purecss.io" class="pure-button pure-button-primary">REPORT</a>
        </p>
    </div>
</div>

<div class="content-wrapper">
    <div class="content">
        <h3 class="content-head is-center">Community Health Disease Tables and Graphs</h3>
        
        
        <div class="pricing-tables pure-g">
        <div class="pure-u-1 pure-u-md-1-3">
            <div class="pricing-table pricing-table-free">
                <div class="pricing-table-header">
                    

                    <span class="pricing-table-price">
                        <h3 style="font-size: .21em; padding-top: 15px;">National Health Issues</h3>
                        <div class="donut-chart-block block"> 
                    
                                        <div class="donut-chart">
                     <!-- PORCIONES GRAFICO CIRCULAR
                          ELIMINADO #donut-chart
                          MODIFICADO CSS de .donut-chart -->
                                    <div id="porcion1" class="recorte"><div class="quesito ios" data-rel="21"></div></div>
                                    <div id="porcion2" class="recorte"><div class="quesito mac" data-rel="39"></div></div>
                                    <div id="porcion3" class="recorte"><div class="quesito win" data-rel="31"></div></div>
                                    <div id="porcionFin" class="recorte"><div class="quesito linux" data-rel="9"></div></div>
                     <!-- FIN AÑADIDO GRÄFICO -->
                                                <p class="center-date">JUNE<br><span class="scnd-font-color">2013</span></p>        
                                        </div>
                            
                    <ul class="os-percentages horizontal-list is-center">
                        <li>
                            <p class="ios os scnd-font-color">Stroke</p>
                            <p class="os-percentage">21<sup>%</sup></p>
                        </li>
                        <li>
                            <p class="mac os scnd-font-color">Heart</p>
                            <p class="os-percentage">39<sup>%</sup></p>
                        </li>
                        <li>
                            <p class="linux os scnd-font-color">TB</p>
                            <p class="os-percentage">9<sup>%</sup></p>
                        </li>
                        <li>
                            <p class="win os scnd-font-color">Pneumonia</p>
                            <p class="os-percentage">31<sup>%</sup></p>
                        </li>
                    </ul>
                                
                </div>
                    </span>
                </div>

                

                <button class="button-choose pure-button">View Crowd Source</button>
            </div>
        </div>

        <div class="pure-u-1 pure-u-md-1-3">
            <div class="pricing-table pricing-table-biz">
                <div class="pricing-table-header">
                    

                    <span class="pricing-table-price">
                        <h3 style="font-size: .21em; padding-top: 15px; color: white;">Leptos Pirose Incidents</h3>
                        
                        <div class="line-chart-block block">
     <div class="line-chart">
       <div class='grafico'>
       <ul class='eje-y'>
         <li data-ejeY='30'></li>
         <li data-ejeY='20'></li>
         <li data-ejeY='10'></li>
         <li data-ejeY='0'></li>
       </ul>
       <ul class='eje-x'>
         <li>Apr</li>
         <li>May</li>
         <li>Jun</li>
           
       </ul>
         <span data-valor='25'>
           <span data-valor='8'>
             <span data-valor='13'>
               <span data-valor='5'>   
                 <span data-valor='23'>   
                 <span data-valor='12'>
                     <span data-valor='15'>
                     </span></span></span></span></span></span></span>
       </div>
         
       
     </div><br>
                    <ul class="os-percentages horizontal-list">
                        <li>
                            <p class="ios os scnd-font-color">Stroke</p>
                            <p class="os-percentage">21<sup>%</sup></p>
                        </li>
                        <li>
                            <p class="mac os scnd-font-color">Heart</p>
                            <p class="os-percentage">39<sup>%</sup></p>
                        </li>
                        <li>
                            <p class="linux os scnd-font-color">TB</p>
                            <p class="os-percentage">9<sup>%</sup></p>
                        </li>
                        
                    </ul>
                    
                </div>
                    </span>
                </div>

                

                <button class="button-choose pure-button">Choose</button>
            </div>
        </div>

        <div class="pure-u-1 pure-u-md-1-3">
            <div class="pricing-table pricing-table-enterprise">
                <div class="pricing-table-header">
                    <h2>Enterprise</h2>

                    <span class="pricing-table-price">
                        <div class="bar-chart-block block">
    <h3 style="font-size: .21em; padding-top: 15px; color: white;">Communicable Diseases</h3>
    <div class='grafico bar-chart'>
       <ul class='eje-y'>
         <li data-ejeY='60'></li>
         <li data-ejeY='45'></li>
         <li data-ejeY='30'></li>
         <li data-ejeY='15'></li>
         <li data-ejeY='0'></li>
       </ul>
       <ul class='eje-x'>
         <li data-ejeX='37'><i>España</i></li>
         <li data-ejeX='56'><i>Portugal</i></li>
         <li data-ejeX='25'><i>Italia</i></li>
         <li data-ejeX='18'><i>Grecia</i></li>
         <li data-ejeX='45'><i>EE.UU</i></li>
         <li data-ejeX='50'><i>México</i></li>
         <li data-ejeX='33'><i>Chile</i></li>
       </ul>
    </div>
  </div>
                    </span>
                </div>

                <ul class="pricing-table-list">
                    <li>Free setup</li>
                    <li>Use your own domain</li>
                    <li>Premium customer support</li>
                    <li>Unlimited file storage</li>
                    <li>25 databases</li>
                    <li>Unlimited bandwidth</li>
                </ul>

                <button class="button-choose pure-button">Choose</button>
            </div>
        </div>
    </div>
    </div>

    <div class="ribbon l-box-lrg pure-g">
        <div class="l-box-lrg is-center pure-u-1 pure-u-md-1-2 pure-u-lg-2-5">
            <img class="pure-img-responsive" alt="File Icons" width="300" src="img/common/file-icons.png">
        </div>
        <div class="pure-u-1 pure-u-md-1-2 pure-u-lg-3-5">

            <h2 class="content-head content-head-ribbon">Laboris nisi ut aliquip.</h2>

            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor.
            </p>
        </div>
    </div>

    <div class="content">
        <h2 class="content-head is-center">Dolore magna aliqua. Uis aute irure.</h2>

        <div class="pure-g">
            <div class="l-box-lrg pure-u-1 pure-u-md-2-5">
                <form class="pure-form pure-form-stacked">
                    <fieldset>

                        <label for="name">Your Name</label>
                        <input id="name" type="text" placeholder="Your Name">


                        <label for="email">Your Email</label>
                        <input id="email" type="email" placeholder="Your Email">

                        <label for="password">Your Password</label>
                        <input id="password" type="password" placeholder="Your Password">

                        <button type="submit" class="pure-button">Sign Up</button>
                    </fieldset>
                </form>
            </div>

            <div class="l-box-lrg pure-u-1 pure-u-md-3-5">
                <h4>Contact Us</h4>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat.
                </p>

                <h4>More Information</h4>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua.
                </p>
            </div>
        </div>

    </div>

    <div class="footer l-box is-center">
        View the source of this layout to learn more. Made with love by the YUI Team.
    </div>

</div>






</body>
</html>
